

----------------------------------------------
-------ESERCIZIO 3 A PARE SIA GIUSTO--------

SELECT C.NomeCampo, C.Citta, A.CodAttivita, COUNT(*) AS ISCRIZOINI
FROM ATTIVITA A, CAMPO_ESTIVO C, ISCRIZIONE I
WHERE I.CodAttivita = A.CodAttivita
AND I.CodCampo = C.CodCampo
AND A.CATEGORIA IN (    SELECT DISTINCT A1.categoria   --- ESTRAGGO ALMENO TRE CATEGORIE
                        FROM ATTIVITA A1, ISCRIZIONE I1
                        WHERE I1.CODATTIVITA=A1.CODATTIVITA   
                        AND I1.CODCAMPO=C.CODCAMPO
                        GROUP BY A1.CATEGORIA
                        HAVING COUNT(*)>=3                       
                        )
 AND  EXISTS( SELECT I1.CODFISCALE,I1.CODCAMPO ---CONTROLLOSS CHE ESISTANO ALMENO 15 RAGAZZI ISCRITTI AL CAMPO ESITIVO
              FROM ISCRIZIONE I1
              WHERE I.CODFISCALE=I1.CODFISCALE
              AND I.CODCAMPO=I1.CODCAMPO
              GROUP BY I1.CODCAMPO,I1.CODFISCALE
              HAVING COUNT(*)>=15)  
GROUP BY C.NomeCampo, C.Citta, A.CodAttivita; 
-------------------------------------------------------------------------------




