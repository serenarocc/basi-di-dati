---------------------------------------------------------
--- ESERCIZIO 1 A GIUSTO----
SELECT G.nome, G.cognome,V.data, COUNT(*) AS TOT_VISITE, SUM(durata) AS TOT_DURATA
FROM tipo_visita T, 
guida G LEFT JOIN visita_guidata_effettuata  V ON G.codguida = V.codguida 
--left join seleziona anche le guide che non hanno fatto nessuna visita
WHERE T.codtipovisita = V.codtipovisita --join per visualizzare anche la durata
AND V.codgr not IN (--gruppi in lingua francese
                      SELECT codgr
                      FROM gruppo
                      WHERE lingua = 'French'
                    )
GROUP BY G.nome, G.cognome, V.data
ORDER BY G.nome, G.cognome, V.data; --ordine facoltativo

------------------------------------------------------------------------
-----PROVE--------

SELECT G.nome, G.cognome,V.data, COUNT(*) AS TOT_VISITE, SUM(durata) AS TOT_DURATA
FROM tipo_visita T, 
guida G , visita_guidata_effettuata  V 
--left join seleziona anche le guide che non hanno fatto nessuna visita
WHERE T.codtipovisita = V.codtipovisita --join per visualizzare anche la durata
AND G.codguida = V.codguida 
AND V.codgr not IN (--gruppi in lingua francese
                      SELECT codgr
                      FROM gruppo
                      WHERE lingua = 'French'
                    )
GROUP BY G.nome, G.cognome, V.data
ORDER BY G.nome, G.cognome, V.data; --ordine facoltativo




--------------------
SELECT*
FROM GRUPPO
ORDER BY LINGUA;

SELECT *
FROM VISITA_GUIDATA_EFFETTUATA



SELECT G.nome, G.cognome,V.data, COUNT(*) AS TOT_VISITE, SUM(durata) AS TOT_DURATA
FROM tipo_visita T, 
guida G LEFT JOIN visita_guidata_effettuata  V ON G.codguida = V.codguida 
--left join seleziona anche le guide che non hanno fatto nessuna visita
WHERE T.codtipovisita = V.codtipovisita --join per visualizzare anche la durata

GROUP BY G.nome, G.cognome, V.data
ORDER BY G.nome, G.cognome, V.data;
---------------------------------------------------------





---------------------------------------

---- EX 1 B GIUSTO--------

SELECT T.monumento
FROM tipo_visita T, visita_guidata_effettuata V, gruppo G
WHERE V.CodTipoVisita = T.CodTipoVisita
AND V.CodGr = G.CodGr
GROUP BY T.Monumento
HAVING SUM(G.NumeroPartecipanti) = (  --SUM serve per poter utilizzare HAVING. Da MAX ci esce 1 numero, questo numero SUM a se stesso da sempre la stessa cosa. � uno escamotage
                                        SELECT MAX(NumVisitatoriTot) --Estraggo il massimo valore del numero dei partecipanti
                                        FROM (  --numero tot di visitatori per Monumento, visitato almeno 10 volte
                                                SELECT   SUM(G1.NumeroPartecipanti) AS NumVisitatoriTot
                                                FROM TIPO_VISITA T1, VISITA_GUIDATA_EFFETTUATA V1, GRUPPO G1
                                                WHERE V1.CodTipoVisita = T1.CodTipoVisita
                                                AND V1.CodGr = G1.CodGr
                                                GROUP BY T1.Monumento
                                                HAVING COUNT(*) >= 10 --visitati almeno 10 volte
                                             ) TOTVISITATORI 
                                   )
                                   
                            
                            
                            
 ----------------------------------------
 
 
 

                            
