//1: trovare i dati relativi a tutti i fattorini della ditta
SELECT *
FROM DELIVERERS; 


//2: trovare gli identificativi di tutte le aziende che hanno usufruito dei servizi della ditta
SELECT DISTINCT COMPANYID
FROM COMPANYDEL;

//3:trovare il nome e il codice di ogni fattorino il cui nome (campo name) inizia con la lettera B
SELECT NAME, DELIVERERID
FROM DELIVERERS
WHERE NAME LIKE 'B%';

//4:Trovare il nome , il sesso e il codice identificativo dei fattorini il cui interno(campo PHONEENO) � diverso da 8467 oppure non esiste
SELECT NAME, SEX, DELIVERERID
FROM DELIVERERS
WHERE PHONENO<>8467 OR PHONENO IS NULL;

//5:Trovare il nome e la citt� di residenza dei fattorini che hanno ricevuto almeno una multa
SELECT DISTINCT NAME,TOWN
FROM DELIVERERS D, PENALTIES P
WHERE D.DELIVERERID=P.DELIVERERID;

//6.Trovare i nomi e le iniziali (attributo INITIALS) dei referenti di azienda che hanno ricevuto almeno un multa dopo il 31/12/1980
// ordinati in ordine alfabetico rispetto al nome
SELECT DISTINCT NAME,INITIALS
FROM DELIVERERS D, PENALTIES P, COMPANIES C
WHERE D.DELIVERERID=P.DELIVERERID AND C.DELIVERERID=P.DELIVERERID
AND P.DATA>TO_DATE('31/12/1980','DD/MM/YYYY')
ORDER BY NAME;


//7: trovare gli identificativi delle coppie formate da un azienda e un fattorino residente a Stratford tra cui ci sono stati almeno due ritiri e una consegna

SELECT C.COMPANYID, D.DELIVERERID
FROM DELIVERERS D, COMPANYDEL C
WHERE D.DELIVERERID=C.DELIVERERID
AND TOWN='Stratford'
AND NUMCOLLECTIONS>=2
AND NUMDELIVERIES>=1;

//8: Trovare gli identificativi dei fattorini(in ordine decrescente) nati dopo il 1962 che hanno effettuato almeno una consegna a un azienda avente il referente al primo mandato
SELECT DISTINCT D.DELIVERERID
FROM DELIVERERS D, COMPANYDEL C1, COMPANIES C2
WHERE D.DELIVERERID=C1.DELIVERERID AND C1.COMPANYID=C2.COMPANYID
AND D.YEAR_OF_BIRTH>'1962'
AND C2.MANDATE='first'
ORDER BY D.DELIVERERID DESC;

//9:Trovare il nome dei fattorini residenti a Inglewood o Stratford che si sono recati presso almeno 2 aziende
SELECT D.NAME
FROM DELIVERERS D, COMPANYDEL C
WHERE D.DELIVERERID=C.DELIVERERID
AND (D.TOWN='Inglewood'  OR D.TOWN='Stratford')
GROUP BY D.DELIVERERID, NAME
HAVING COUNT (*)>1;


//11: per tutti i fattorini che hanno ricevuto almeno 2 multe e non piu di 4, trovare il nome del fattorino e la multa minima pagata
SELECT D.NAME, MIN(P.AMOUNT)
FROM DELIVERERS D,PENALTIES P
WHERE D.DELIVERERID=P.DELIVERERID
GROUP BY D.DELIVERERID,D.NAME 
HAVING COUNT(*)>=2 AND COUNT(*)<=4;

//12: Trovare il numero totale di consegne e il numero totale di ritiri effettuati da fattorini non residenti a Stratford il cui cognome (campo NAME) inizia con B
SELECT SUM (NUMDELIVERIES), SUM(NUMCOLLECTIONS)
FROM DELIVERERS D, COMPANYDEL C
WHERE D.DELIVERERID=C.DELIVERERID
AND TOWN<>'Stratford'
AND NAME LIKE 'B%';
