# Basi di Dati / Database



La cartella Basi di Dati contiene lo svolgimento di alcuni progetti svolti durante il corso di Basi di Dati del Politecnico di Torino nell'anno 2020.

Linguaggi utilizzati: SQL, HTML, PHP.

L'ide impiegato è ORACLE SQL Developer.


-------------------------------------------------
The Databases directory contains some projects carried out during the course of Databases at the Politecnico di Torino in 2020.

Languages used: SQL, HTML, PHP.

Ide used: ORACLE SQL Developer.
The code is introduced with the notepad++ editor and saved in a file with the extension .s. The file can also be saved with the extension .a or .asm

## Link ad altri miei progetti personali / Links to other personal projects

- [ ] [Tecniche di Programmazione/ Programming Techniques](https://gitlab.com/serenarocc/tecniche-di-programmazione-linguaggio-c)
- [ ] [Algoritmi linguaggio C / Algorithms](https://gitlab.com/serenarocc/algoritmi-linguaggio-c)
- [ ] [Algoritmi e Strutture Dati / Algorithms and Data Structures](https://gitlab.com/serenarocc/algoritmi-e-strutture-dati-linguaggio-c)
- [ ] [Sistemi Operativi/ Operating Systems](https://gitlab.com/serenarocc/sistemi-operativi) 
- [ ] [Calcolatori Elettronici/Computer architecture](https://gitlab.com/serenarocc/calcolatori-elettronici-linguaggio-assembly-mips)
- [ ] [Basi di Dati /Databases](https://gitlab.com/serenarocc/basi-di-dati)
- [ ] [Programmazione a oggetti /Object Oriented Programming](https://gitlab.com/serenarocc/object-oriented-programming-java)
```
