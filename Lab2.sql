-- 1  trovare codice identificativo, nome e iniziali (campo INITIALS)
-- dei fattorini che non hanno mai preso multe
    SELECT DELIVERERID,NAME,INITIALS
    FROM DELIVERERS
    WHERE DELIVERERID NOT IN(SELECT  DELIVERERID
                            FROM PENALTIES);
                            
    --oppure
    SELECT DELIVERERID,NAME,INITIALS
    FROM DELIVERERS D 
    WHERE NOT EXISTS ( SELECT*
                        FROM PENALTIES P
                        WHERE P.DELIVERERID=D.DELIVERERID);
  
                        
-- 2 trovare il codice identificativo di tutti i fattorini che hanno ricevuto
-- almeno una multa da 25euro e almeno una multa da 30euro
    select distinct DELIVERERID 
    FROM PENALTIES
    WHERE AMOUNT =25
    AND delivererid IN (SELECT DELIVERERID 
                        FROM PENALTIES
                        WHERE AMOUNT =30);
    --oppure
    SELECT DELIVERERID 
    FROM DELIVERERS
    WHERE DELIVERERID  IN (SELECT DELIVERERID 
                            FROM PENALTIES
                            WHERE AMOUNT=25)
    AND DELIVERERID IN (SELECT DELIVERERID 
                            FROM PENALTIES
                            WHERE AMOUNT=30);                    


-- 3 trovare il codice identificativo e nome dei fattorini
-- che nella stessa data hanno ricevuto piu di una multa
SELECT DISTINCT D.DELIVERERID , NAME
FROM DELIVERERS D, PENALTIES P
WHERE D.DELIVERERID=P.DELIVERERID
GROUP BY D.DELIVERERID, DATA, NAME
HAVING COUNT(*)>1;


--4 trovare il codice identificativo dei fattorini che si sono recati
-- presso tutte le aziende presenti nella tabella COMPANIES
SELECT DELIVERERID
FROM COMPANYDEL
GROUP BY DELIVERERID
HAVING COUNT(*)=(SELECT COUNT (*)
                 FROM COMPANIES);


-- 5 trovare il codice identificativo dei fattorini che hanno fatto consegne(o ritiri)
--in almeno un'azienda in cui il fattorino 57 ha fatto delle consegne(o dei ritiri)
 SELECT DISTINCT DELIVERERID
 FROM COMPANYDEL
 WHERE DELIVERERID<>57
 AND COMPANYID IN (SELECT COMPANYID
                     FROM COMPANYDEL
                     WHERE DELIVERERID=57);


--6 trovare il codice identificativo e nome dei fattorini per cui il numero di multe
--ricevute nel 1980 � superiore al numero di multe ricevute nel 1981
SELECT D.DELIVERERID, D.NAME
FROM DELIVERERS D, PENALTIES PE1
WHERE D.DELIVERERID=PE1.DELIVERERID
AND DATA>=TO_DATE('01/01/1980','DD/MM/YYYY')
AND DATA<=TO_DATE('31/12/1980','DD/MM/YYYY')
GROUP BY D.DELIVERERID, D.NAME
HAVING COUNT(*)>(SELECT COUNT(*)
                FROM PENALTIES PE2
                WHERE D.DELIVERERID=PE2.DELIVERERID
                AND DATA>=TO_DATE('01/01/1981','DD/MM/YYYY')
                AND DATA<=TO_DATE('31/12/1981','DD/MM/YYYY'));
                
                
-- 7 trovare il codice identificaativo del fattorino che ha ricevuto 
-- il numero massimo di multe
SELECT DELIVERERID
FROM PENALTIES
GROUP BY DELIVERERID
HAVING COUNT(*)=(SELECT MAX(NumPenalties)
                 FROM ( SELECT DELIVERERID,COUNT(*) AS NumPenalties
                        FROM PENALTIES
                        GROUP BY  DELIVERERID)DELIVERERID
                );
--oppure
SELECT DELIVERERID
FROM ( SELECT DELIVERERID,COUNT(*) AS NumPenalties
       FROM PENALTIES
       GROUP BY DELIVERERID)TOTMULTEDELIVERERS1
WHERE NumPenalties=(SELECT MAX(NumPenalties)
                    FROM(SELECT DELIVERERID,COUNT(*) AS NumPenalties
                    FROM PENALTIES
                    GROUP BY DELIVERERID)TOTMULTEDELIVERERS2
                    );
                    
                    
--8 trovare il codice identificativo dei fattorini che hanno fatto consegne (o ritiri)
--in TUTTE le aziende in cui il fattorino 57 ha fatto delle consegne (o dei ritiri)
SELECT DELIVERERID
FROM COMPANYDEL
WHERE DELIVERERID<>57
AND COMPANYID IN(SELECT COMPANYID 
                 FROM COMPANYDEL
                 WHERE DELIVERERID=57)
GROUP BY DELIVERERID
HAVING COUNT (*)=(SELECT COUNT(*)
                  FROM COMPANYDEL
                  WHERE DELIVERERID=57);


--9 trovare il codice identificativo dei fattorini che hanno fatto consegne
--(o ritiri) SOLO nelle aziende in cui il fattorino 57 ha fatto delle consegne(o dei ritiri)
SELECT DISTINCT DELIVERERID
FROM COMPANYDEL
WHERE DELIVERERID<>57
AND DELIVERERID NOT IN(  SELECT DELIVERERID
                         FROM COMPANYDEL
                         WHERE COMPANYID NOT IN (SELECT COMPANYID
                                                 FROM COMPANYDEL
                                                 WHERE DELIVERERID=57));


--10 trovare il codice identificativo dei fattorini che hanno fatto consegne (o ritiri) in
-- tutte e sole le aziende in cui il fattorino 57 ha fatto delle consegne (o dei ritiri)
SELECT DISTINCT DELIVERERID
FROM COMPANYDEL
WHERE DELIVERERID<>57
AND DELIVERERID NOT IN(  SELECT DELIVERERID
                         FROM COMPANYDEL
                         WHERE COMPANYID NOT IN (SELECT COMPANYID
                                                 FROM COMPANYDEL
                                                 WHERE DELIVERERID=57))
GROUP BY DELIVERERID
HAVING COUNT(*)=(SELECT COUNT(*)
                 FROM COMPANYDEL
                 WHERE DELIVERERID=57);




