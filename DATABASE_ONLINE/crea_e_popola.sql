-- create an empty database. Name of the database:
-- SET storage_engine=InnoDB;
SET FOREIGN_KEY_CHECKS=1; 
CREATE DATABASE IF NOT EXISTS CICLISTI 
CHARACTER SET utf8mb4;
 
-- USE CICLISTI
USE CICLISTI;
 
 -- drop=cancella
-- drop tables if they already exist
DROP TABLE IF EXISTS CLASSIFICA_INDIVIDUALE;
DROP TABLE IF EXISTS CICLISTA;
DROP TABLE IF EXISTS SQUADRA;
DROP TABLE IF EXISTS TAPPA;

 
-- create tables
CREATE TABLE SQUADRA (
    CodS CHAR(50) NOT NULL,
    NomeS CHAR(50) NOT NULL,
    AnnoFondazione INTEGER NOT NULL,
    SedeLegale CHAR(50) NULL,
	PRIMARY KEY(CodS),
    CONSTRAINT chk_AnnoFondazione CHECK (AnnoFondazione>=1900 AND AnnoFondazione<=2000)
);
 
CREATE TABLE CICLISTA (
    CodC INTEGER NOT NULL,
    Nome CHAR(50) NOT NULL,
    Cognome CHAR(50) NOT NULL,
    Nazionalita CHAR(50) NOT NULL,
    CodS CHAR(50) NOT NULL,
    AnnoNascita INTEGER NOT NULL,
    PRIMARY KEY(CodC),
	FOREIGN KEY (CodS)
        REFERENCES SQUADRA(CodS)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT chk_AnnoNascita CHECK (AnnoNascita>=1900 AND AnnoNascita<=2000)
);
 
CREATE TABLE TAPPA (
    Edizione INTEGER NOT NULL,
    CodT INTEGER NOT NULL,   
    CittaPartenza CHAR(50) NOT NULL,
    CittaArrivo CHAR(50) NOT NULL,
    Lunghezza INTEGER NOT NULL,
    Dislivello INTEGER NOT NULL,
    GradoDifficolta SMALLINT NOT NULL,
    PRIMARY KEY (Edizione, CodT),
    CONSTRAINT chk_GradoDifficolta CHECK (GradoDifficolta>=1 AND GradoDifficolta<=10), 
    CONSTRAINT chk_CodT CHECK (CodT>=1),
    CONSTRAINT chk_Edizione CHECK (Edizione>=1)
);
 
CREATE TABLE CLASSIFICA_INDIVIDUALE (
    CodC INTEGER NOT NULL, 	
    CodT INTEGER NOT NULL,  
    Edizione INTEGER NOT NULL, 
    Posizione INTEGER NOT NULL,
    PRIMARY KEY (CodC,CodT,Edizione),
    FOREIGN KEY (CodC)
        REFERENCES CICLISTA(CodC)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    FOREIGN KEY (Edizione,CodT)
        REFERENCES TAPPA(Edizione,CodT)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);


-- SET storage_engine=InnoDB;
SET FOREIGN_KEY_CHECKS=1;
USE CICLISTI;

-- Insert data

INSERT INTO SQUADRA (CodS,NomeS,AnnoFondazione,SedeLegale)
VALUES ('s1','girasole',1911,'Torino');
INSERT INTO SQUADRA (CodS,NomeS,AnnoFondazione,SedeLegale)
VALUES ('s2','violetta',1910,'Biella');
INSERT INTO SQUADRA (CodS,NomeS,AnnoFondazione,SedeLegale)
VALUES ('s3','margherita',1922,'Firenze');
INSERT INTO SQUADRA (CodS,NomeS,AnnoFondazione,SedeLegale)
VALUES ('s4','rosa',1933,'Palermo');
INSERT INTO SQUADRA (CodS,NomeS,AnnoFondazione,SedeLegale)
VALUES ('s5','calle',1981,'Pisa');


INSERT INTO CICLISTA (CodC,Nome,Cognome,Nazionalita,CodS,AnnoNascita)
VALUES (1,'Angela','Simons','italiana','s1',1994);
INSERT INTO CICLISTA (CodC,Nome,Cognome,Nazionalita,CodS,AnnoNascita)
VALUES (2,'Armando','Cremone','italiana','s1',1964);
INSERT INTO CICLISTA (CodC,Nome,Cognome,Nazionalita,CodS,AnnoNascita)
VALUES (3,'Mirco','Romolo','italiana','s2',1944);
INSERT INTO CICLISTA (CodC,Nome,Cognome,Nazionalita,CodS,AnnoNascita)
VALUES (4,'Enea','Maralu','italiana','s2',1960);
INSERT INTO CICLISTA (CodC,Nome,Cognome,Nazionalita,CodS,AnnoNascita)
VALUES (5,'Benedetta','Cimone','italiana','s3',1966);
INSERT INTO CICLISTA (CodC,Nome,Cognome,Nazionalita,CodS,AnnoNascita)
VALUES (6,'Anna','Zorzi','italiana','s3',1972);
INSERT INTO CICLISTA (CodC,Nome,Cognome,Nazionalita,CodS,AnnoNascita)
VALUES (7,'Giulia','Palermo','italiana','s4',1996);
INSERT INTO CICLISTA (CodC,Nome,Cognome,Nazionalita,CodS,AnnoNascita)
VALUES (8,'Valentina','Demarchi','italiana','s4',1986);
INSERT INTO CICLISTA (CodC,Nome,Cognome,Nazionalita,CodS,AnnoNascita)
VALUES (9,'Massimo','Pericolo','italiana','s5',1995);
INSERT INTO CICLISTA (CodC,Nome,Cognome,Nazionalita,CodS,AnnoNascita)
VALUES (10,'Federica','Romaniolo','italiana','s5',1919);


INSERT INTO TAPPA (Edizione,CodT,CittaPartenza,CittaArrivo,Lunghezza,Dislivello,GradoDifficolta)
VALUES (1,1,'Chieti','Roma',23456,15,2);
INSERT INTO TAPPA (Edizione,CodT,CittaPartenza,CittaArrivo,Lunghezza,Dislivello,GradoDifficolta)
VALUES (1,2,'Roma','Siena',65432,60,7);
INSERT INTO TAPPA (Edizione,CodT,CittaPartenza,CittaArrivo,Lunghezza,Dislivello,GradoDifficolta)
VALUES (1,3,'Siena','Cremona',12345,34,5);
INSERT INTO TAPPA (Edizione,CodT,CittaPartenza,CittaArrivo,Lunghezza,Dislivello,GradoDifficolta)
VALUES (1,4,'Cremona','Torino',54321,22,4);

INSERT INTO TAPPA (Edizione,CodT,CittaPartenza,CittaArrivo,Lunghezza,Dislivello,GradoDifficolta)
VALUES (2,1,'Chieti','Roma',23456,15,2);
INSERT INTO TAPPA (Edizione,CodT,CittaPartenza,CittaArrivo,Lunghezza,Dislivello,GradoDifficolta)
VALUES (2,2,'Roma','Siena',65432,60,7);
INSERT INTO TAPPA (Edizione,CodT,CittaPartenza,CittaArrivo,Lunghezza,Dislivello,GradoDifficolta)
VALUES (2,3,'Siena','Cremona',12345,34,5);
INSERT INTO TAPPA (Edizione,CodT,CittaPartenza,CittaArrivo,Lunghezza,Dislivello,GradoDifficolta)
VALUES (2,4,'Cremona','Torino',54321,22,4);


INSERT INTO CLASSIFICA_INDIVIDUALE (CodC,CodT,Edizione,Posizione)
VALUES (1,1,1,1);
INSERT INTO CLASSIFICA_INDIVIDUALE (CodC,CodT,Edizione,Posizione)
VALUES (2,1,1,2);
INSERT INTO CLASSIFICA_INDIVIDUALE (CodC,CodT,Edizione,Posizione)
VALUES (3,1,1,3);

INSERT INTO CLASSIFICA_INDIVIDUALE (CodC,CodT,Edizione,Posizione)
VALUES (3,2,1,1);
INSERT INTO CLASSIFICA_INDIVIDUALE (CodC,CodT,Edizione,Posizione)
VALUES (2,2,1,2);
INSERT INTO CLASSIFICA_INDIVIDUALE (CodC,CodT,Edizione,Posizione)
VALUES (1,2,1,3);

INSERT INTO CLASSIFICA_INDIVIDUALE (CodC,CodT,Edizione,Posizione)
VALUES (4,1,2,4);
INSERT INTO CLASSIFICA_INDIVIDUALE (CodC,CodT,Edizione,Posizione)
VALUES (5,1,2,2);
INSERT INTO CLASSIFICA_INDIVIDUALE (CodC,CodT,Edizione,Posizione)
VALUES (6,1,2,3);
INSERT INTO CLASSIFICA_INDIVIDUALE (CodC,CodT,Edizione,Posizione)
VALUES (1,1,2,1);

