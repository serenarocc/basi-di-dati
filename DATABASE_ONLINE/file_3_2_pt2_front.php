
<!DOCTYPE html>

<html>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> 

<body>
<h1>  Inserimento ciclista in tappa</h1>

<form name='Ins_ciclista_in tappa' action="file_3_2_pt2_back.php">
  <label for="CodC">Codice ciclsta:</label><br>
  <select id="CodC" name="CodC" class="w3-select">
  <option value=''>Seleziona ciclista</option>

 <?php

        /* Establish DB connection */
        $conn = @mysqli_connect ( 'localhost', 'root', '', 'CICLISTI' );

        if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error ();
        } 

        /* Query construction */
        $query = "SELECT CodC FROM CICLISTA";
        
        /* Query execution */
        $result = mysqli_query ( $conn, $query );
        if (!$result){
            die ( 'Query error: ' . mysqli_error ( $conn ) );
        }

        /* Check if course found */
        if (mysqli_num_rows ( $result ) > 0) {
            while($row = mysqli_fetch_array($result)) {
                $ciclista_code = $row["CodC"];
                echo "<option value='$ciclista_code'>$ciclista_code</option>";
            }
        }

    ?>
</select>


<label for="Edizione">Edizione:</label><br>
    <select id="Edizione" name="Edizione" class="w3-select">
    <option value=''>Seleziona Edizione</option>

 <?php

        /* Query construction */
        $query = "SELECT DISTINCT Edizione FROM TAPPA";
        
        /* Query execution */
        $result = mysqli_query ( $conn, $query );
        if (!$result){
            die ( 'Query error: ' . mysqli_error ( $conn ) );
        }

        /* Check if found */
        if (mysqli_num_rows ( $result ) > 0) {
            while($row = mysqli_fetch_array($result)) {
                $Edizione = $row["Edizione"];
                echo "<option value='$Edizione'>$Edizione</option>";
            }
        }

    ?>
</select>

<label for="Tappa">Tappa:</label><br>
    <select id="CodT" name="CodT" class="w3-select">
    <option value=''>Seleziona Tappa</option>

 <?php

        /* Query construction */
        $query = "SELECT DISTINCT CodT FROM TAPPA";
        
        /* Query execution */
        $result = mysqli_query ( $conn, $query );
        if (!$result){
            die ( 'Query error: ' . mysqli_error ( $conn ) );
        }

        /* Check if found */
        if (mysqli_num_rows ( $result ) > 0) {
            while($row = mysqli_fetch_array($result)) {
                $CodT = $row["CodT"];
                echo "<option value='$CodT'>$CodT</option>";
            }
        }

    ?>
</select>


<label for="Posizione">Posizione:</label><br>
<input type='text' name='Posizione' size='15' value='Posizione'/>
</br>


<input type="submit" value="Aggiungi">
<input type="Reset" value="Cancella">


</form>

</body>

</html>