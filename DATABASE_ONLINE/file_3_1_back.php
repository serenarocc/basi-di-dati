<!DOCTYPE html> 
<html>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> 
<body>

<?php

/* Verify parameter ciclista_code */
if (!isset($_REQUEST["ciclista_code"]) || trim($_REQUEST["ciclista_code"]) == ""){
	echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Codice corso mancante o campo vuoto.</p>
    </div>";
    exit;
}

/* Verify parameter tappa */
if (!isset($_REQUEST["tappa"]) || trim($_REQUEST["tappa"]) == ""){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Codice tappa mancante o campo vuoto.</p>
    </div>";
    exit;
}


$ciclista_code = $_REQUEST["ciclista_code"];
$tappa         = $_REQUEST["tappa"];   


/* Establish DB connection */
$conn = @mysqli_connect ( 'localhost', 'root', '', 'CICLISTI' );

if (mysqli_connect_errno()) {
	echo "Failed to connect to MySQL: " . mysqli_connect_error ();
} 


/* String sanification for DB query */
$ciclista_code = mysqli_real_escape_string($conn, $ciclista_code); 
$tappa         = utf8_encode(utf8_decode( mysqli_real_escape_string($conn, $tappa) )); 


/* Check tappa  */
if (!is_numeric($tappa )){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>La durata deve essere un valore numerico.</p>
    </div>";
    exit;
}


/* Check ciclista_code  */
if (!is_numeric($ciclista_code )){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>La durata deve essere un valore numerico.</p>
    </div>";
    exit;
}


/* Query construction */
$query = "SELECT C.Nome, C.Cognome, S.NomeS, CI.edizione, CI.codt, CI.Posizione
			FROM CICLISTA C, SQUADRA S, TAPPA T, CLASSIFICA_INDIVIDUALE CI
			WHERE C.CodS=S.CodS 
			and CI.CodC=C.CodC
			and CI.CodT=T.CodT
			AND CI.Edizione=T.Edizione
			AND C.CodC='$ciclista_code'
			AND CI.CodT='$tappa'
			ORDER BY CI.edizione";
			
			
/* Query execution */
$result = mysqli_query ( $conn, $query );
if (!$result){
    die ( 'Query error: ' . mysqli_error ( $conn ) );
}

/* Check if course found */
if (mysqli_num_rows ( $result ) > 0) {
    echo "<h1>Dettagli ciclista $ciclista_code nella tappa $tappa</h1>";
    echo "<table class='w3-table-all w3-hoverable'>";

    /* Table header */
    echo "<thead><tr>";
    $i = 0;
    $field_names = [];
    while($i<mysqli_num_fields($result)) { 
        $meta=mysqli_fetch_field($result); 
        echo "<th>".$meta->name."</th>"; 
        array_push($field_names, $meta->name);
        $i++; 
    } 
    echo "</thead></tr>";
    
    /* Table content */
    while($row = mysqli_fetch_array($result)) {
        echo "<tr>";
        foreach ($field_names as $field){
            /* String sanification for HTML */
            $safe_html = htmlspecialchars($row[$field], ENT_QUOTES | ENT_SUBSTITUTE, 'utf-8');
            echo "<td>" . $safe_html . "</td>";
        }
        echo "</tr>";
    }
    echo "</table>";

} else {
    echo "<h1>Ciclista $ciclista_code non trovato nella tappa $tappa .</h1>";
}

?>


</body>
</html>