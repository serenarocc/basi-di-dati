<!DOCTYPE html>   
<html>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> 
<body>

<?php

/* Verify parameter codc */
if (!isset($_REQUEST["Cod_c"]) || trim($_REQUEST["Cod_c"]) == ""){  
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Codice ciclista mancante o vuoto.</p>
    </div>";
    exit;
}

/* Verify parameter nomec */
if (!isset($_REQUEST["Nome_c"]) || trim($_REQUEST["Nome_c"]) == ""){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Nome mancante o vuoto.</p>
    </div>";
    exit;
}

/* Verify parameter cognomec */
if (!isset($_REQUEST["Cognome_c"]) || trim($_REQUEST["Cognome_c"]) == ""){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Cognome mancante o vuoto.</p>
    </div>";
    exit;
}

/* Verify parameter nazionalita */
if (!isset($_REQUEST["Nazionalità"]) || trim($_REQUEST["Nazionalità"]) == ""){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Nazionalità mancante o vuoto.</p>
    </div>";
    exit;
}
/* Verify parameter cods */
if (!isset($_REQUEST["Cod_s"]) || trim($_REQUEST["Cod_s"]) == ""){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p> Codice squadra mancante o vuoto.</p>
    </div>";
    exit;
}
/* Verify parameter anno */
if (!isset($_REQUEST["Anno_nascita"]) || trim($_REQUEST["Anno_nascita"]) == ""){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Anno di nascita vuoto.</p>
    </div>";
    exit;
}

$Cod_c=$_REQUEST["Cod_c"];
$Nome_c=$_REQUEST["Nome_c"];
$Cognome_c=$_REQUEST["Cognome_c"];
$Cod_s=$_REQUEST["Cod_s"];
$Nazionalità=$_REQUEST["Nazionalità"];
$Anno_nascita=$_REQUEST["Anno_nascita"];


/* Establish DB connection */
$conn = @mysqli_connect ( 'localhost', 'root', '', 'CICLISTI' );

if (mysqli_connect_errno()) {
	echo "Failed to connect to MySQL: " . mysqli_connect_error ();
} 


/* String sanification for DB query */
$Cod_c       = utf8_encode(utf8_decode( mysqli_real_escape_string($conn, $Cod_c) )); 
$Nome_c      = utf8_encode(utf8_decode( mysqli_real_escape_string($conn, $Nome_c) )); 
$Cognome_c   = utf8_encode(utf8_decode( mysqli_real_escape_string($conn, $Cognome_c) )); 
$Cod_s       = utf8_decode( mysqli_real_escape_string($conn, $Cod_s) ); 
$Nazionalità = utf8_encode(utf8_decode( mysqli_real_escape_string($conn, $Nazionalità) )); 
$Anno_nascita= utf8_encode(utf8_decode( mysqli_real_escape_string($conn, $Anno_nascita) )); 




//check integrità dei dati

/* Check CodC è intero  */
if (!is_numeric($Cod_c )){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Il CodC deve essere un valore numerico.</p>
    </div>";
    exit;
}


/* Check Anno_nascita è intero  */
if (!is_numeric($Anno_nascita)){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Il Anno Nascita deve essere un valore numerico.</p>
    </div>";
    exit;
}


//controllo su cod Squadra 

$query="SELECT *
         FROM SQUADRA
		 WHERE CodS='$Cod_s';";
		 
$result=mysqli_query($conn, $query);

if (!$result){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Inserimento non riuscito! ". mysqli_error ( $conn ) ." </p>
    </div>";
	exit;
}elseif (mysqli_num_rows ( $result ) == 0) {
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>La squadra $Cod_s non esiste.</p>
    </div>";
    exit;
}
   


//controllo che non ci siano ciclisti con lo stesso codice

$query="SELECT *
         FROM CICLISTA
		 WHERE CodC='$Cod_c';";
		 
$result=mysqli_query($conn, $query);

if (!$result){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Inserimento non riuscito! ". mysqli_error ( $conn ) ." </p>
    </div>";
	exit;
}elseif (mysqli_num_rows ( $result ) != 0) {
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>$Cod_c esiste già.</p>
    </div>";
    exit;
}		 


//costruzione query per l'inserimento
$query="INSERT INTO CICLISTA(CodC,Nome,Cognome,CodS,Nazionalita,AnnoNascita)
        VALUES('$Cod_c', '$Nome_c', '$Cognome_c', '$Cod_s', '$Nazionalità', '$Anno_nascita');";


//esecuzione query
$result = mysqli_query ( $conn, $query );
if (!$result){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Inserimento del ciclista $Cod_c non riuscito! ". mysqli_error ( $conn ) ." </p>
    </div>";
    exit;
} else {
    echo "<div class='w3-panel w3-green'>
        <h3>Congratulazioni!</h3>
        <p>Inserimento del ciclista $Cod_c riuscito.</p>
    </div>  ";
    exit;
}
?>

</body>
</html>