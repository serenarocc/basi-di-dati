<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> 
<body>

<?php

/* Verify parameter course_code */
if (!isset($_REQUEST["CodC"]) || trim($_REQUEST["CodC"]) == ""){  
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Codice ciclista mancante o vuoto.</p>
    </div>";
    exit;
}

/* Verify parameter course_name */
if (!isset($_REQUEST["Edizione"]) || trim($_REQUEST["Edizione"]) == ""){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Edizione mancante o vuoto.</p>
    </div>";
    exit;
}

/* Verify parameter course_type */
if (!isset($_REQUEST["CodT"]) || trim($_REQUEST["CodT"]) == ""){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Tappa mancante o vuoto.</p>
    </div>";
    exit;
}

/* Verify parameter course_level */
if (!isset($_REQUEST["Posizione"]) || trim($_REQUEST["Posizione"]) == ""){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Posizione mancante o vuoto.</p>
    </div>";
    exit;
}

$Posizione  = $_REQUEST["Posizione"];
$CodC  = $_REQUEST["CodC"];
$CodT  = $_REQUEST["CodT"];
$Edizione = $_REQUEST["Edizione"];

/* Establish DB connection */
$conn = @mysqli_connect ( 'localhost', 'root', '', 'CICLISTI' );

if (mysqli_connect_errno()) {
	echo "Failed to connect to MySQL: " . mysqli_connect_error ();
} 

/* String sanification for DB query */
$Posizione =  utf8_decode( mysqli_real_escape_string($conn, $Posizione)  ); 
$CodC =  utf8_decode( mysqli_real_escape_string($conn, $CodC)  ); 
$CodT =  utf8_decode( mysqli_real_escape_string($conn, $CodT)  ); 
$Edizione = utf8_decode( mysqli_real_escape_string($conn, $Edizione) ); 



//check integrità dei dati


/* Check Posizione è intero  */
if (!is_numeric($Posizione )){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Posizione deve essere un valore numerico.</p>
    </div>";
    exit;
}

/* Check CodC è intero. Controllo inutile  */
if (!is_numeric($CodC )){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>CodC deve essere un valore numerico.</p>
    </div>";
    exit;
}


/* Check CodT è intero. Controllo inutile  */
if (!is_numeric($CodT )){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>CodT deve essere un valore numerico.</p>
    </div>";
    exit;
}

/* Check Edizione è intero. Controllo inutile  */
if (!is_numeric($Edizione )){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Edizione deve essere un valore numerico.</p>
    </div>";
    exit;
}

//cod ciclista (controllo inutle perchè il ciclista è stato scelto dai dati della base di dati)

$query="SELECT *
         FROM CICLISTA
		 WHERE CodC='$CodC';";
		 
$result=mysqli_query($conn, $query);
if (!$result){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Inserimento non riuscito! ". mysqli_error ( $conn ) ." </p>
    </div>";
	exit;
}elseif (mysqli_num_rows ( $result ) == 0) {
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>$CodC non esiste.</p>
    </div>";
    exit;
}
   


//Edizione (controllo inutle perchè Edizione è stato scelto dai dati della base di dati)

$query="SELECT *
         FROM TAPPA
		 WHERE Edizione='$Edizione';";
$result=mysqli_query($conn, $query);
if (!$result){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Inserimento non riuscito! ". mysqli_error ( $conn ) ." </p>
    </div>";
	exit;
}elseif (mysqli_num_rows ( $result ) == 0) {
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>$Edizione non esiste.</p>
    </div>";
    exit;
}		 


//Codice Tappa (controllo inutle perchè Codice Tappa è stato scelto dai dati della base di dati)

$query="SELECT *
         FROM TAPPA
		 WHERE CodT='$CodT';";
$result=mysqli_query($conn, $query);
if (!$result){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Inserimento non riuscito! ". mysqli_error ( $conn ) ." </p>
    </div>";
	exit;
}elseif (mysqli_num_rows ( $result ) == 0) {
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>$CodT non esiste.</p>
    </div>";
    exit;
}	





//Controllo che per l'edizione selezionata esista la Tappa selezionata

$query="SELECT CodT
         FROM TAPPA
		 WHERE Edizione='$Edizione';";
		 
$result=mysqli_query($conn, $query);
if (!$result){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Inserimento non riuscito! ". mysqli_error ( $conn ) ." </p>
    </div>";
	exit;
}elseif (mysqli_num_rows ( $result ) == 0) {
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>$CodT non esiste nell'edizione $Edizione.</p>
    </div>";
    exit;
}	




//Controllo che non esista già una posizione per il ciclista nella tappa selezionata

$query="SELECT CodC
         FROM CLASSIFICA_INDIVIDUALE
		 WHERE CodT='$CodT' and Edizione='$Edizione';";
		 
$result=mysqli_query($conn, $query);
if (!$result){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Inserimento non riuscito! ". mysqli_error ( $conn ) ." </p>
    </div>";
	exit;
}else 
{
	$check=false;
	while($row=mysqli_fetch_assoc($result)){
  if ($check==false){
	if($row["CodC"]==$CodC)  
	 $check=true; 
  }
  }
  if($check==true)
  {echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>$CodC esiste già nella tappa '$CodT' dell'edizione $Edizione.</p>
    </div>";
  exit;}
}





//inseriamo i dati
$query="INSERT INTO CLASSIFICA_INDIVIDUALE(CodC, CodT, Edizione, Posizione)
        VALUES('$CodC','$CodT','$Edizione','$Posizione');";

//esecuzione query
$result = mysqli_query ( $conn, $query );
if (!$result){
    echo "<div class='w3-panel w3-red'>
        <h3>Errore!</h3>
        <p>Inserimento del ciclista $CodC in posizione $Posizione non riuscito! ". mysqli_error ( $conn ) ." </p>
    </div>";
    exit;
} else {
    echo "<div class='w3-panel w3-green'>
        <h3>Congratulazioni!</h3>
        <p>Inserimento del ciclista $CodC in posizione $Posizione riuscito.</p>
    </div>  ";
    exit;
}
?>

</body>
</html>