<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> 
<body>

<h1>Posizione Ciclista in Tappa</h1>

<form action="file_3_1_back.php">
    <label for="ciclista_code">Codice Ciclista:</label>
    <select id="ciclista_code" name="ciclista_code" class="w3-select">
    <option value=''>Seleziona un codice ciclista</option>
    <?php

        /* Establish DB connection */
        $conn = @mysqli_connect ( 'localhost', 'root', '', 'CICLISTI' );

        if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error ();
        } 

        /* Query construction */
        $query = "SELECT CodC FROM CICLISTA";
        
        /* Query execution */
        $result = mysqli_query ( $conn, $query );
        if (!$result){
            die ( 'Query error: ' . mysqli_error ( $conn ) );
        }

        /* Check if course found */
        if (mysqli_num_rows ( $result ) > 0) {
            while($row = mysqli_fetch_array($result)) {
                $ciclista_code = $row["CodC"];
                echo "<option value='$ciclista_code'>$ciclista_code</option>";
            }
        }

    ?>
    </select>
    <br>
    <br>
	
	
    <label for="tappa">Codice Tappa:</label>
    <input type="text" id="tappa" name="tappa">  <br><br>
	
    <input type="submit" value="Cerca">
</form> 


</body>
</html>