-- 1 trovare per ogni fattorino che ha preso almeno due multe il codice identificativo del fattorino,
--la data della prima multa e la data dell'ultima multa che ha preso
    SELECT DELIVEREDID, MIN(DATA), MAX(DATA)
	FROM PENALTIES
	GROUP BY DELIVEREDID
	HAVING COUNT(*)>=2;


-- 2  trovare per ogni fattorino che ha preso almeno una multa il codice identificativo del fattorino,
-- la data in cui ha preso l'ultima multa e l'ammontare della mmulta
   SELECT P1.DELIVEREDID, DATA, AMOUNT
   FROM PENALTIES P1
   WHERE P1.DATA = (SELECT MAX(DATA)
					FROM PENALTIES P2
					WHERE P2. DELIVEREDID=P1.DELIVEREDID);

-- 3  Trovare l�identificativo delle aziende presso cui si sono recati pi� del 30% dei fattorini presenti
--nella base di dati (nota: i fattorini �recatisi� presso un�azienda sono quelli che hanno fatto almeno
--una consegna o un ritiro presso l�azienda in esame).
   SELECT COMPANYID
   FROM COMPANYDEL
   GROUP BY COMPANYID
   HAVING COUNT(*) > ( SELECT 0.30*COUNT(*)
					   FROM DELIVERERS );
